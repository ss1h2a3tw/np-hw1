#include <cstdio>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <unistd.h>
#include <cstring>
#include <errno.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <algorithm>
#include <set>
#include <queue>
#include <list>
//Using C++11

using namespace std;

//const string prompt=">";
const string prompt="% "; //NP
const vector<string> SEPR = {">>",">&","<&","<",">","&","|","!",";"};
const vector<string> RDIR = {">>",">&","<&",">","<"};
enum rdir_type {APPEND_OUT,DUP_OUT,DUP_IN,OUT,IN};
const vector<string> BLTIN = {"cd","setenv","unset","printenv","exit","fg"};
enum bltn_type {CD,SETENV,UNSET,PRINTENV,EXIT,FG};
//TODO redirect filedescripter [n]> word
//TODO << redirect
//TODO fg
const vector<string> PIPE = {"|","!"};
enum pipe_type {PIPE_OUT,PIPE_OUTERR};

const string mktemp_template = "/tmp/myshell.XXXXXX";

vector<pair<pid_t,string>> cpgid;
bool interactive=false;
list<pair<size_t,string>> np;

struct redirect{
    rdir_type type;
    string path;
};

struct command{
    string exe;
    vector<string> args;
    vector<redirect> redirects;
    bool piped;
    pipe_type piped_type;
    size_t pipecnt;
    bool builtin;
    bltn_type builtin_type;
    bool background;
    void clear(){
        exe="";
        args.clear();
        redirects.clear();
        piped=false;
        pipecnt=0;
        background=false;
        builtin=false;
    }
};

size_t insertpid(pid_t pid,const string& str){
    auto num=cpgid.size();
    for(size_t i = 0 ; i < num ; i ++){
        if(cpgid[i].first==0){
            num=i;
            cpgid[i].first=pid;
            cpgid[i].second=str;
        }
    }
    if(num==cpgid.size()){
        cpgid.push_back({pid,str});
    }
    return num;
}
void checkslash(const command& now){
    for(auto& x:now.args){
        if(any_of(x.begin(),x.end(),[](char x){return x=='/';}))throw("For Security Reasons no / in arguments permitted");
    }
}
void checkredirect(const command& now){
    for(auto& x:now.redirects){
        for(auto& y:SEPR){
            if(x.path==y)throw("Expect from/to");
            //TODO single quote
        }
    }
}
size_t expand(string& in,size_t now , size_t pred){
    string name = in.substr(pred+1,now-pred-1);
    in.erase(pred,now-pred);
    auto env = getenv(name.c_str());
    if(env!=NULL)in.insert(pred,getenv(name.c_str()));
    return pred;
}
vector<string> gettokens(string& in ){
    vector<string> tokens;
    size_t pre = 0;
    size_t pred = 0;
    bool needexpand=false;
    for(size_t i = 0 ; i < in.size() ; i ++){
        if(in[i]=='$'){
            if(needexpand){
                i=expand(in,i,pred);
                needexpand=false;
                continue;
            }
            pred=i;
            needexpand=true;
        }
        if(isspace(in[i])){
            if(needexpand){
                i=expand(in,i,pred);
                needexpand=false;
                continue;
            }
            if(i-pre>0)tokens.push_back(in.substr(pre,i-pre));
            pre=i+1;
            continue;
        }
        for(auto& x:SEPR){
            if(in.compare(i,x.size(),x)==0){
                if(needexpand){
                    i=expand(in,i,pred);
                    needexpand=false;
                    break;
                }
                if(i-pre>0){
                    tokens.push_back(in.substr(pre,i-pre));
                }
                tokens.push_back(in.substr(i,x.size()));
                i+=x.size()-1;
                pre=i+1;
                break;
            }
        }

    }
    return tokens;
}

command formcommand(const vector<string>::const_iterator begin , const vector<string>::const_iterator end){
    command now;
    now.clear();
    for(auto it = begin ; it != end ; it++){
        bool getredir=false;
        for(size_t i = 0 ; i < RDIR.size() ; i ++){
            if(*it==RDIR[i]){
                if(it==begin)throw("Expect command but got redirect");
                if(it+1==end)throw("No redirect from/to");
                redirect tmp;
                tmp.type=(rdir_type)i;
                tmp.path = *(it+1);
                now.redirects.push_back(move(tmp));
                it++;
                getredir=true;
                break;
            }
        }
        if(getredir)continue;
        if(it==begin){
            now.exe = *it;
            for(size_t i = 0 ; i < BLTIN.size() ; i ++){
                if(now.exe==BLTIN[i]){
                    now.builtin=true;
                    now.builtin_type=(bltn_type)i;
                }
            }
        }
        else{
            now.args.push_back(*it);
        }
    }
    checkredirect(now);
    checkslash(now);
    return now;
}

vector<command> getcommands(const vector<string>& tokens){
    auto pre = tokens.begin();
    vector<command> now;
    for(auto it = tokens.begin() ; it != tokens.end() ; it++){
        for(size_t i = 0 ; i < PIPE.size() ; i ++){
            if(PIPE[i]==*it){
                if(it==pre)throw("Expect command but got pipe");
                command tmp=formcommand(pre,it);
                tmp.piped=true;
                tmp.piped_type=(pipe_type)i;
                now.push_back(move(tmp));
                pre=it+1;
            }
            if(all_of(pre->cbegin(),pre->cend(),[](char c){return c>='0'&&c<='9';})){
                int tmp=stoi(*pre);
                if(tmp<=0)throw("Invalid number of numberpipe");
                (now.end()-1)->pipecnt=(size_t)tmp;
                pre++;
                it++;
            }
            continue;
        }
        if(*it=="&"){
            if(pre==it)throw("Expect command but found &");
            command tmp=formcommand(pre,it);
            tmp.piped=false;
            tmp.background=true;
            now.push_back(move(tmp));
            pre=it+1;
            continue;
        }
        if(*it==";"){
            if(pre==it){
                pre=it+1;
                continue;
            }
            command tmp=formcommand(pre,it);
            tmp.piped=false;
            now.push_back(move(tmp));
            pre=it+1;
            continue;
        }
    }
    return now;
}
vector<vector<command>> pseperate(const vector<command>& commands){
    vector<vector<command>> rtn;
    vector<command> tmp;
    for(auto& now :commands){
        if(tmp.size()!=0&&now.builtin==true)throw("Find buildin commands in pipe");
        if(now.piped&&now.builtin==true)throw("Find builtin commands in pipe");
        tmp.push_back(now);
        if(!now.piped||(now.piped&&now.pipecnt>0)){
            rtn.push_back(tmp);
            tmp.clear();
        }
    }
    if(tmp.size()!=0){
        throw("Expect commands after non number pipes");
    }
    return rtn;
}
void simpleexec(const command& cmd,int errfd){
    try{
        signal(SIGCHLD,SIG_DFL);
        char* argv[cmd.args.size()+2];
        argv[0] = new char[cmd.exe.size()];
        argv[cmd.args.size()+1]=NULL;
        strncpy(argv[0],cmd.exe.c_str(),cmd.exe.size());
        argv[0][cmd.exe.size()]=0;
        for(size_t i = 0 ; i < cmd.args.size() ; i ++){
            argv[i+1] = new char[cmd.args[i].size()+1];
            strncpy(argv[i+1],cmd.args[i].c_str(),cmd.args[i].size());
            argv[i+1][cmd.args[i].size()]=0;
        }
        for(size_t i = 0 ; i < cmd.redirects.size() ; i ++){
            int newfd=0;
            switch(cmd.redirects[i].type){
                case APPEND_OUT:
                    newfd=open(cmd.redirects[i].path.c_str(),O_WRONLY|O_CREAT|O_APPEND,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
                    if(newfd==-1)throw("Failed to open file");
                    dup2(newfd,STDOUT_FILENO);
                    close(newfd);
                    break;
                case DUP_OUT:
                    break;
                case DUP_IN:
                    break;
                case OUT:
                    newfd=open(cmd.redirects[i].path.c_str(),O_WRONLY|O_CREAT|O_TRUNC,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
                    if(newfd==-1)throw("Failed to open file");
                    dup2(newfd,STDOUT_FILENO);
                    close(newfd);
                    break;
                case IN:
                    newfd=open(cmd.redirects[i].path.c_str(),O_RDONLY);
                    if(newfd==-1)throw("Failed to open file");
                    dup2(newfd,STDIN_FILENO);
                    close(newfd);
                    break;
            }
        }
        int rtn=execvp(cmd.exe.c_str(),argv);
        for(size_t i = 0 ; i <= cmd.args.size() ; i ++){
            delete argv[i];
        }
        if(rtn==-1){
            if(errno==ENOENT){
                throw("Unknown command: ["+cmd.exe+"].");
            }
            throw("Error while executing commands");
        }
    }
    catch (const char* err){
        write(errfd,err,strlen(err));
    }
    catch (const string& err){
        write(errfd,err.c_str(),err.size());
    }
    // If we get here , it means we didn't exec normally
    exit(EXIT_FAILURE);
}

pid_t modfdfork(const command& cmd,int pgid,int fdin,int fdout,int fderr = STDERR_FILENO){
    pid_t pid;
    int errfd[2];
    pipe2(errfd,O_CLOEXEC);
    while((pid=fork())==-1){
        wait(NULL);
    }
    if(pid==0){
        setpgid(0,pgid);
        dup2(fdin,STDIN_FILENO);
        dup2(fdout,STDOUT_FILENO);
        dup2(fderr,STDERR_FILENO);
        auto chk=[](int fd){
            if(fd!=STDIN_FILENO&&fd!=STDOUT_FILENO&&fd!=STDERR_FILENO)
                return true;
            return false;
        };
        if(chk(fdin))close(fdin);
        if(chk(fdout))close(fdout);
        if(chk(fderr))close(fderr);
        simpleexec(cmd,errfd[1]);
    }
    if(pgid==0)
        setpgid(pid,pid);
    else setpgid(pid,pgid);
    close(errfd[1]);
    char buf[1000]={0};
    if(read(errfd[0],buf,999)!=0){
        close(errfd[0]);
        throw(buf);
    }
    close(errfd[0]);
    return pid;
}

void killchild(){
    signal(SIGTERM,SIG_IGN);
    kill(getpid(),SIGTERM);
    exit(EXIT_SUCCESS);
}

void builtin_cd(const command& cmd){
    if(cmd.args.size()==0){
        const char* homepath = getenv("HOME");
        if(homepath==NULL)throw("Failed to get home path");
        if(chdir(getenv("HOME"))==-1)throw("Error in chdir()");
        return;
    }
    if(cmd.args.size()>1)throw("cd:Too many arguments");
    if(chdir(cmd.args[0].c_str())==-1)throw("Error in chdir()");
}

void builtin_setenv(const command& cmd){
    if(cmd.args.size()!=2)throw("Usage: setenv name value");
    if(setenv(cmd.args[0].c_str(),cmd.args[1].c_str(),1)==-1)throw("Error in setenv()");
}
void builtin_unset(const command& cmd){
    if(cmd.args.size()!=1)throw("Usage: unset name");
    if(unsetenv(cmd.args[0].c_str())==-1)throw("Error in unsetenv()");
}
void builtin_printenv(const command& cmd){
    if(cmd.args.size()!=1)throw("Usage: printenv name");
    char* tmp=getenv(cmd.args[0].c_str());
    if(tmp==NULL)throw("No such Variable");
    cout << cmd.args[0] << "=" << tmp << endl;
}
void builtin_fg(const command& cmd){
    auto& args = cmd.args;
    if(!interactive)throw("fg is not available in non interactive mode");
    if(cmd.args.size()!=1)throw("Usage: fg Jobnum");
    if(!all_of(args[0].begin(),args[0].end(),[](char c){return c>='0'&&c<='9';}))
        throw("Usage: fg Jobnum");
    int i = stoi(args[0]);
    if(i<=0||(size_t)i>cpgid.size())throw("Jobnum not exist");
    i--;
    if(cpgid[i].first==0)throw("Jobnum not exist");
    cout << "Sent Jobnum " <<i+1<<" "<<cpgid[i].second << " to foreground" << endl;
    tcsetpgrp(STDIN_FILENO,cpgid[i].first);
    kill(-cpgid[i].first,SIGCONT);
    while(1){
        waitpid(-cpgid[i].first,NULL,0);
        if(errno==ECHILD)break;
    }
    tcsetpgrp(STDIN_FILENO,getpid());
}
void builtin_exit(){
    for(auto& x:np){
        if(remove(x.second.c_str())==-1)throw("Error in remove numberpipe tmpfile");
        np.pop_front();
    }
    killchild();
}
int ckrunbuiltin(const command& cmd){
    if(cmd.builtin){
        switch(cmd.builtin_type){
            case CD:
                builtin_cd(cmd);
                break;
            case SETENV:
                builtin_setenv(cmd);
                break;
            case UNSET:
                builtin_unset(cmd);
                break;
            case PRINTENV:
                builtin_printenv(cmd);
                break;
            case EXIT:
                builtin_exit();
                break;
            case FG:
                builtin_fg(cmd);
                break;
        }
        return 1;
    }
    return 0;
}
pid_t moderrfork(const command& cmd,int pgid, int _fdin, int _fdout){
    if(cmd.piped&&cmd.piped_type==PIPE_OUTERR){
        return modfdfork(cmd,pgid,_fdin,_fdout,_fdout);
    }
    else return modfdfork(cmd,pgid,_fdin,_fdout);
};
pid_t execsingle(const vector<command>& cmds,int fdin,int fdout){
    pid_t pgid=moderrfork(cmds[0],0,fdin,fdout);
    return pgid;
}
pid_t execpipe(const vector<command>& cmds,int fdin,int fdout){
    int pfd[2];
    if(pipe(pfd)==-1)throw("Failed to pipe");
    int pre=pfd[0];
    pid_t pgid=moderrfork(cmds[0],0,fdin,pfd[1]);
    close(pfd[1]);
    for(size_t i = 1 ; i < cmds.size()-1 ; i ++){
        if(pipe(pfd)==-1)throw("Failed to pipe");
        moderrfork(cmds[i],pgid,pre,pfd[1]);
        close(pre);
        close(pfd[1]);
        pre=pfd[0];
    }
    moderrfork(cmds[cmds.size()-1],pgid,pre,fdout);
    close(pre);
    return pgid;
}
int chkinnp(){
    if(np.size()==0)return STDIN_FILENO;
    if(np.begin()->first!=0)return STDIN_FILENO;
    int fdin=open(np.begin()->second.c_str(),O_RDONLY);
    if(fdin==-1)throw("Failed openning tempfile");
    return fdin;
}
void decnp(){
    if(np.size()==0)return;
    if(np.begin()->first==0){
        if(remove(np.begin()->second.c_str())==-1)throw("Error in remove numberpipe tmpfile");
        np.pop_front();
    }
    for(auto &x:np)x.first--;
}
void insertnp(size_t cnt, string&& path){
    auto it = np.begin();
    for(;it!=np.end()&&it->first<cnt;it++);
    if(it==np.end()){
        np.insert(it,{cnt,path});
        return;
    }
    if(it->first==cnt)throw("Two numberpipes to same line");
    np.insert(it,{cnt,path});
}
int outnp(size_t cnt){
    auto it = np.begin();
    for(;it!=np.end()&&it->first<cnt;it++);
    if(it!=np.end()&&it->first==cnt){
        int fd=open(it->second.c_str(),O_WRONLY|O_CREAT|O_APPEND,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
        if(fd==-1)throw("Failed to open tmpfile to write");
        return fd;
    }
    char tmp[mktemp_template.size()+1];
    strcpy(tmp,mktemp_template.c_str());
    int fdout = mkstemp(tmp);
    if(fdout==-1)throw("Failed to create tmpfile");
    np.insert(it,{cnt,string(tmp)});
    return fdout;
}
int main (){
    signal(SIGINT,[](int){
        cout<<"^C"<<endl;
    });
    signal(SIGCHLD,[](int){
        waitpid(-1,NULL,WNOHANG);
        queue<size_t> toerase;
        for(size_t i = 0 ; i < cpgid.size() ; i ++){
            if(waitpid(-cpgid[i].first,NULL,WNOHANG)==-1&&errno==ECHILD){
                toerase.push(i);
            }
        }
        while(!toerase.empty()){
            cerr << endl << "Job " << toerase.front()+1 << ":" << cpgid[toerase.front()].second << " has ended" << endl;
            cpgid[toerase.front()].first=0;
            toerase.pop();
        }
    });
    signal(SIGTTOU,SIG_IGN);
    if(setpgid(0,getpid())==-1)throw("Failed to setpgid");
    if(tcsetpgrp(STDIN_FILENO,getpid())==-1)interactive=false;
    else interactive=true;
    while(1){
        try{
            cout.flush();
            cout << prompt;
            string in;
            getline(cin,in);
            in = in + ' ';
            vector<string> tokens=gettokens(in);
            tokens.push_back(";");
            vector<command> commands=getcommands(tokens);
            vector<vector<command>> pscommands=pseperate(commands);
            for(auto& cmds:pscommands){
                bool background=cmds[cmds.size()-1].background;
                decnp();
                int fdin=chkinnp();
                int fdout=STDOUT_FILENO;
                if((cmds.end()-1)->pipecnt>0){
                    fdout=outnp((cmds.end()-1)->pipecnt);
                }
                if(ckrunbuiltin(cmds[0]))continue;
                pid_t pgid;
                if(cmds.size()==1)pgid=execsingle(cmds,fdin,fdout);
                else pgid=execpipe(cmds,fdin,fdout);
                if(!background){
                    if(interactive)
                        if(tcsetpgrp(STDOUT_FILENO,pgid)==-1)
                            throw("Error setting terminal pgrp");
                    while(1){
                        if(waitpid(pgid*-1,NULL,0)==-1&&errno==ECHILD)break;
                    }
                    if(interactive)
                        if(tcsetpgrp(STDOUT_FILENO,getpid())==-1)
                            throw("Error setting terminal pgrp");
                }
                else insertpid(pgid,cmds[0].exe);
                if(fdin!=STDIN_FILENO)close(fdin);
                if(fdout!=STDOUT_FILENO)close(fdout);
            }
        }
        catch (const char *err){
            cerr << err << endl;
        }
        catch (const string& err){
            cerr << err << endl;
        }
    }
}
