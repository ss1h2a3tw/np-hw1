#include <cstdio>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <iostream>
#include <typeinfo>
#include <cstring>

using namespace std;

void execshell(int fd){
    string home=getenv("HOME");
    if(setenv("PATH","bin:.",1)==-1)throw("Error in setenv()");
    if(dup2(fd,STDIN_FILENO)==-1)throw("Error in dup2()");
    if(dup2(fd,STDOUT_FILENO)==-1)throw("Error in dup2()");
    if(dup2(fd,STDERR_FILENO)==-1)throw("Error in dup2()");
    cout << "****************************************" << endl;
    cout << "** Welcome to the information server. **" << endl;
    cout << "****************************************" << endl;
    if(chdir((home+"/ras").c_str())==-1)throw("Error in chdir()");
    char shell[100];
    strcpy(shell,(home+"/shell").c_str());
    execl(shell,shell);
}

int main (){
    try{
        int fd=socket(AF_INET,SOCK_STREAM,0);
        const int enable = 1;
        if(setsockopt(fd,SOL_SOCKET,SO_REUSEADDR,&enable,sizeof(enable))==-1)throw("Error setting socket");
        sockaddr_in addr;
        addr.sin_family=AF_INET;
        addr.sin_port=htons(8787);
        addr.sin_addr.s_addr=INADDR_ANY;
        if(fd==-1)throw("Error opening socket");
        if(bind(fd,(sockaddr*)&addr,sizeof(sockaddr_in))==-1)throw("Failed to bind socket");
        while(1){
            listen(fd,10);
            socklen_t clilen=sizeof(sockaddr_in);
            int newfd=accept(fd,(sockaddr*)&addr,&clilen);
            if(newfd==-1)throw("Failed to accept new client");
            cout << "Connect from" << inet_ntoa(addr.sin_addr) << ":" << ntohs(addr.sin_port) << endl;
            int pid=fork();
            if(pid==-1)throw("Failed to fork");
            if(pid==0){
                execshell(newfd);
            }
            else close(newfd);
        }
    }
    catch(const char* err){
        cerr << err << endl;
    }
}
